(ns kindle-notes.app
  (:require [kindle-notes.core])
  (:gen-class))

(defn dump-to-db
  [dir]
  (println "Not yet implemented."))

(defn -main
  ([action & args]
     (case (keyword action)
       :dump (apply dump-to-db args)
       ;; Default
       (kindle-notes.core/start!)))
  
  ([] (-main :run)))