(ns kindle-notes.db
  (:require [korma.db :as db :refer (defdb)]
            [korma.core :as korma :refer :all]))

(defdb kindle-notes (db/sqlite3 {:db "resources/kindle.db"
                                 }))

(defentity notes
  (database kindle-notes)
  
  (entity-fields :title
                 :author
                 :page
                 :type
                 :location)

  (transform (fn [{note-type :type :as v}]
               (if (keyword? note-type)
                 (assoc v :type (name note-type))
                 v))))

(defn init-table
  []
  (let [conn (db/get-connection kindle-notes)]
    (exec-raw conn ["CREATE TABLE notes (title TEXT, author TEXT, page TEXT, type TEXT, location TEXT)"])))

(comment
  (init-table)
  
  (insert notes
          (values [{:title "Testing"
                    :author "Sanders, Brian"
                    :page "235"
                    :type :note
                    :location "234"}]))


  )