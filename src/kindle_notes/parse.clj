(ns kindle-notes.parse
  (:require [clojure.java.io :refer (reader)]
            [clojure.string :as str]

            [clj-time.format :as time :refer (formatter)]))

(def k-time-formatter
  (formatter "EEEE, MMMM dd, yyyy, hh:mm aa"))

(def k-time-display
  (formatter "MMM dd, yyyy"))

(defn notes [lines]
  (->> lines
       (partition-by #(= "==========" %))
       (remove #(= (first %) "=========="))))

(defn parse-title
  "Takes a title string, returns the title and author."
  [title]
  (let [m (re-matcher #" \((.*)\)$" title)]
    (if (.find m)
      [(subs title 0 (.start m)) (.group m 1)]
      ;;
      [title])))

(defn- parse-info-line
  [line]
  (re-find #"Your (\w+)(?: on Page (\d+) \|)? Location (\d+(?:-\d+)?) \| Added on (.*)" line))

(defn parse-time
  [t]
  (try
    (time/parse k-time-formatter t)

    (catch Exception e
      nil)))

(defn parse-info
  "Takes an info string, returns the page number, location, timestamp,
  and type (highlight/note)."
  [info]
  (let [[_ ntype pg loc dt] (parse-info-line info)]
    {:page pg
     :type (if ntype (keyword (str/lower-case ntype)))
     :location loc
     :date (parse-time dt)}))

(defn note-to-map
  [[title info _ & note]]
  (let [[title author] (parse-title title)
        info (parse-info info)]
    (assoc info
      :title title
      :author author
      :note (str/join "\n" note))))

(defn unique-keyvals
  [coll key]
  (into #{} (map key coll)))

(defn books-with-title
  [])

(defn note-name
  [notemap]
  (time/unparse k-time-display (:date notemap)))

(defn get-notes
  [rdr]
  (notes (line-seq rdr)))

(defn get-note-maps
  [rdr]
  (map note-to-map (get-notes rdr)))

(defn note-reader
  []
  (reader "resources/clippings.txt"))

(comment
  (with-open [rdr (note-reader)]
    (prn (take 5 (get-note-maps rdr))))

  (parse-time )

  (unique-keyvals [{:name "Jimbo"} {:name "Jimbo"} {:name "joe"}] :name)

  (parse-info-line "- Your Highlight on Page 91 | Location 1476-1476 | Added on Monday, January 16, 2012, 05:35 PM")
  )