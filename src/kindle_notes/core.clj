(ns kindle-notes.core
  (:require [clojure.java.io :refer (reader)]

            [seesaw.bind :as b]
            [seesaw.core :as seesaw :refer (config! select)]
            [seesaw.mig :refer (mig-panel)]

            [kindle-notes.parse :as notes]))

(seesaw/native!)

(def extracted-notes (atom nil))

(defn load-notes
  "Loads notes from the given file path."
  [fpath]
  (with-open [rdr (reader fpath)]
    (let [notes (doall (notes/get-note-maps rdr))]
      (group-by :title notes))))

(defn load-notes!
  [fpath]
  (let [notes (load-notes fpath)]
    (swap! extracted-notes (constantly notes))))

(defn make-app-frame
  []
  (seesaw/frame :title "Kindle Notes"
                :menubar nil
                ;; :resizable? false
                :content (mig-panel :id :content
                                    :constraints ["width 650::, height 300::", "", ""]
                                    :items [[(seesaw/scrollable
                                              (seesaw/listbox :id :books)) "width 250, height 100%, span 1 2"]
                                            [(seesaw/scrollable
                                              (seesaw/listbox :id :notes
                                                              :renderer (fn [r note]
                                                                          (config! r :text (:name (:value note))))))
                                             "width 200, height 100%, span 1 2"]
                                            [(seesaw/scrollable
                                              (seesaw/text :id :note-content
                                                           :multi-line? true
                                                           :editable? false
                                                           :wrap-lines? true))
                                             "width 150::, height 80%, growx, wrap"]
                                            [(seesaw/button :text "Load Notes"
                                                            :listen [:action (fn [_]
                                                                               (load-notes! "resources/clippings.txt"))])
                                             "height 40"]])))


(defn add-bindings
  [fr]
  (let [books (select fr [:#books])]
    (b/bind extracted-notes
            (b/transform keys)
            (b/property books :model))

    (b/bind (b/selection books)
            (b/transform (fn [v]
                           (map #(hash-map :name (notes/note-name %)
                                           :pos %2)
                                (get @extracted-notes v)
                              (range))))
            (b/property (select fr [:#notes]) :model))

    (b/bind (b/selection (select fr [:#notes]))
            (b/transform (fn [{pos :pos}]
                           (let [title (seesaw/selection books)]
                             (get-in @extracted-notes [title pos]))))
            (b/tee
             (b/bind (b/transform :note) (select fr [:#note-content]))
             ))))

(defn start!
  []
  (let [fr (make-app-frame)]
    (-> fr
        (seesaw/pack!)
        (seesaw/show!)
        (add-bindings))
    fr))


(comment
  (def *fr (start!))
  
  (load-notes! "resources/clippings.txt")
  (keys @extracted-notes)

  )

