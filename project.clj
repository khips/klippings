(defproject kindle-notes "0.1.0-SNAPSHOT"
  :description "Simple application for examining my Kindle notes."
  :url "http://statori.com/kindle-notes"
  :license {:name "Eclipse Public License"
            :url "http://www.eclipse.org/legal/epl-v10.html"}
  :main kindle-notes.app
  :jvm-opts ["-Xdock:name=My Kindle Notes"]
  :dependencies [[org.clojure/clojure "1.5.1"]
                 [seesaw "1.4.3"]
                 [korma "0.3.0-RC5"]
                 [clj-time "0.6.0"]
                 [org.xerial/sqlite-jdbc "3.7.2"]])
